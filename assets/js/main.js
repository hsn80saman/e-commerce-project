$(document).ready(function () {
  $(".owl-carousel").owlCarousel({
    rtl: true,
    slideBy: 3,
    items: 5,
    loop: 5000,
    margin: 0,
    autoplay: true,
    autoplayTimeout: 2500,
    autoplayHoverPause: true,
    responsiveClass: true,
  });
});
